import React, {useEffect,useState} from 'react';
import { request } from '../lib/datocms'

import Head from 'next/head'

import {css} from '@emotion/core';
import BackgroundWrapper from '../components/BackgroundWrapper';
import About from '../components/About';
import ProjectsGallery from '../components/ProjectsGallery';

import Footer from '../components/Footer';
import Overlay from '../components/Overlay'
import Menu from '../components/Menu'

const HOMEPAGE_QUERY = `
  query homePage {
    allCerts {
      certificateimg {
        id
        url
        responsiveImage(imgixParams: {auto: format, fit: crop}) {
          base64
          bgColor
          title
          alt
          aspectRatio
          srcSet
          src
          sizes
          height
          width
          webpSrcSet
        }
      }
    }
    allProyectos {
      id
      title
      img {
        url
        responsiveImage(imgixParams: {fit: crop, auto: format}) {
          base64
          bgColor
          title
          alt
          aspectRatio
          srcSet
          src
          sizes
          height
          width
          webpSrcSet
        }
      }
      technologies
      url
      state
      category
    }
}`

export async function getStaticProps() {
  const data = await request({
    query: HOMEPAGE_QUERY,
    variables: { limit: 10 }
  });
  return {
    props: { data }
  };
}

export default function Home({data}) {

  return (
    <div >
      <Head>
        <title>Johan G. Book</title>
        <link rel="icon" href="/favicon.ico" />
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@200;400;700&display=swap" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mixitup/3.3.1/mixitup.min.js"></script>
      </Head>
      <Menu/>
      <Overlay />
      <BackgroundWrapper 
          css={css`position:relative;`}
      />

      <main css={css``}>
        <About allCerts={data.allCerts} />
        <ProjectsGallery 
          allProyectos={data.allProyectos}
        />

      </main>
      <Footer />
      
    </div>
  )
}
