import React from 'react';
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import styles from '../styles/Home.module.css'
import { AiOutlineGitlab } from "react-icons/ai";
import { AiOutlineLinkedin } from "react-icons/ai";

const Footer = () => {

    const Brand = styled.div`
        margin-top:.5rem;
        text-align:center;
        display:flex;
        justify-content:space-around;
        & p {
            color:rgba(255,255,255,0.5)
        }
    `;

    const SocialM = styled.div`
        display:flex;
        justify-content:center;

        & a {

        }

        & a div {
            display:flex;
            justify-content:space-around;
            margin:1rem;
            background:#ebe9e9;
            padding:.5rem;
            transition: all .3s ease-in;
        }
        & a div:hover {
            transform:scale(0.93);
            background:#3FBF3F;
        }

    `;


    return ( 
        <footer className={styles.footer} id="contact">
            <SocialM >
                <a
                    href="https://gitlab.com/johan208"
                    target="_blank"
                >
                    <div>
                        <AiOutlineGitlab
                            size="30px"
                            color={"#000"}
                        />
                    </div>
                </a>
                <a
                    href="https://www.linkedin.com/in/johan-garcia-57b5bb175/"
                    target="_blank"
                >
                    <div>
                        <AiOutlineLinkedin 
                            size="30px"
                            color="#000"
                        />
                    </div>
                </a>
            </SocialM>
            <Brand>
                <div>
                    <p><i>johangr.development@gmail.com</i></p>
                    <p> <i>Johan García &nbsp;
                        <span css={css`color:#3FBF3F;`}>&#169; 2020</span>
                    </i></p>
                </div>
                
            </Brand>
            
        </footer>
     );
}
 
export default Footer;