import React, { useEffect, useRef, useState} from 'react';
import {css } from '@emotion/core';

import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import {MenuBurger} from './styled'

const Menu = () => {

    // const [menuOpened,setmenuOpened] = useState(false)
    const burgerColor = useRef(null)

    useEffect( () => {
        if (typeof window !== `undefined`) {
            gsap.registerPlugin(ScrollTrigger)
        }
        let tl = gsap.timeline( {
            ease:'power2.ease',
        });
        gsap.fromTo(burgerColor.current, {backgroundColor: 'rgba(0,0,0,0)'}, {
            duration:0.5,
            backgroundColor:'rgba(63,191,63,1)',
            ease:'none',
            scrollTrigger: {
                trigger:'#about',
                start:'top +=200',
                end:'top',
                // markers:true,
                scrub: true
            }
        })

        
    }, [])

    const manuBurgerWhite = css`
        background-color: #ffffff;
    `;

    const closeMenuOnClick = () => {
        const toggler = document.querySelector('.toggler');
        toggler.click();
    }
    return ( 
        <MenuBurger className="CinzelTitle">
            <input type="checkbox" className="toggler" /> 
            <div 
                className="hamburger"
                ref={burgerColor}
            >
                <div
                    css={[manuBurgerWhite]}
                >

                </div>
            </div>

            <div className="menu">
                <div className="video-container">
                    <video autoPlay muted loop>
                        <source src="/static/hellow.mp4" />
                    </video>
                </div>
                <nav>
                    <ul>
                        <li><a href="#home" onClick={closeMenuOnClick}>Home</a></li>
                        <li><a href="#about" onClick={closeMenuOnClick}>About</a></li>
                        <li><a href="#projects" onClick={closeMenuOnClick}>Projects</a></li>
                        <li><a href="#contact" onClick={closeMenuOnClick}>Contact</a></li>
                        
                    </ul>
                </nav>

            </div>


        </MenuBurger>
     );
}
 
export default Menu;