import styled from '@emotion/styled';

export const MenuBurger = styled.div`
  position:fixed;
  top:0px;
  right:60px;
  z-index:1000;

  .toggler {
  position:absolute;
  top:0;
  left:0;
  z-index:1000;
  cursor:pointer;
  opacity:0;
  width:50px;
  height:50px; 

      &:checked + .hamburger {
          clip-path: polygon(50% 0%, 100% 50%, 50% 100%, 0% 50%);
      }


      &:checked + .hamburger > div {
          transform: rotate(135deg);
      }

      &:checked + .hamburger > div:before,
      &:checked + .hamburger > div:after{
          top:0;
          transform: rotate(90deg);
          transition:all 0.5s ease-in;
      }

      &:checked ~ .menu {
          /* transform: translateX(0px); */
          visibility:visible;
          opacity:1; 
          z-index:-1;
          
          /* Landscape */
          
          @media only screen
          
          and (min-device-width: 280px)
          
          and (max-device-width: 667px)
          
          and (orientation: landscape) {
          
              width:100% !important;
              height:100% !important;
          }
      }
      &:not(:checked) ~ .menu {
          opacity:0;
          visibility:hidden;
          z-index:-1;
      }
      &:checked ~ .menu ul li {
          opacity:1; 
          transform: translateX(0px);
      }
      &:checked ~ .menu ul li:hover a {
        background-position:-100%;
      }
      &:not(:checked) ~ .menu ul li {
          opacity:0;
          transform: translateX(-200px);
          
      }

  }

  .hamburger {
      position:absolute;
      top:0;
      left:0;
      z-index:1;
      height:60px;
      width:60px;
      padding:1rem;
      background:rgba(63,191,63,1);
      display:flex;
      align-items:center;
      justify-content:center; 
      transition:all 0.5s ease-in-out;
      
      clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%);
  }

  .hamburger > div {
      position:relative;
      width:100%;
      height:2px;
      display:flex;
      align-items:center;
      justify-content:center;
      transition:all 0.5s ease-in-out;
  }
  .hamburger > div:before,
  .hamburger > div:after {
      content:'';
      position:absolute;
      z-index:1;
      top:-10px;
      width:100%;
      height:2px;
      background:inherit;
      
  }

  .hamburger > div:after {
      top:10px;
  }

  .menu {
      position:fixed;
      top:0;
      right:0;
      opacity:0;
      width:100vw;
      height:100vh;
      overflow:hidden;
      display:grid;
      grid-template-columns:70vw 30vw;
      transition: all .6s ease-in-out;
      z-index:-11;
      visibility:hidden;
      backdrop-filter:blur(15px);
      /* transform: translateX(0px); */

      video {
          position:relative;
          height:100vh;
          width:100%;
          object-fit:cover;
      }
      .video-container {
          position:relative;
          width:100%;
          height:100%;
          
      }
      .video-container:after {
        content:'';
        position:absolute;
        top:0;
        left:0;
        width:100%;
        height:100%;
        background-color:rgba(15,17 ,8,.9);
        mix-blend-mode:overlay;
        
      }

      & nav {
        position:relative;
        width:100%;
        height:100%;
        background-color:rgba(0,0 ,0,.8);
        mix-blend-mode:overlay;
      }
      & ul{
        height:100%;
        width:100%;
        display:flex;
        flex-direction:column;
        align-items:center;
        justify-content:center;
        padding:0;
        margin:0;
        z-index:11111;
      }
      & ul li {
          list-style:none;
          color:white;
          font-size:calc(.6rem + 2vw);
          font-weight:bold;
          padding:.6rem 1rem;
          width:100%;
          opacity:0;
          text-align:center;
          transition: transform 0.3s ease-in;
      }
      & ul li:hover {
          transform:translateY(20px);
      }
      & ul li:nth-of-type(1n) {
        transition: all 0.22s cubic-bezier(0.83, 0, 0.17, 1);
      }
      & ul li:nth-of-type(2n) {
        transition: all .2s 0.22s cubic-bezier(0.83, 0, 0.17, 1);
      }
      & ul li:nth-of-type(3n) {
        transition: all .4s 0.22s cubic-bezier(0.83, 0, 0.17, 1);
      }
      & ul li:nth-of-type(4n) {
        transition: all .6s 0.22s cubic-bezier(0.83, 0, 0.17, 1);
      }
      
      & ul li a{
          text-decoration:none;
          color:white;
          cursor:pointer;
          font-weight:bold;
          text-transform:uppercase;
          background:linear-gradient(to right,#DEDEDE 50%,rgba(63,191,63,1) 50%, rgba(63,191,63,1));
          background-size:200%;
          -webkit-background-clip: text;
          -webkit-text-fill-color:transparent;
          /* text-shadow: 2px 2px 5px rgba(10, 10, 10, .5); */
          transition: 0.37s ease-in-out;
      }
      @media only screen

        and (min-device-width: 280px)

        and (max-device-width: 768px)

        and (orientation: portrait) {

            grid-template-columns:0 100vw !important;
            
            & ul li {
                font-size:calc(1rem + 2vw);
            }
        }

        /* Landscape */

        @media only screen

        and (min-device-width: 280px)

        and (max-device-width: 667px)

        and (orientation: landscape) {

        }
  }
`;