import React, {useState, useEffect, useRef} from 'react';
import {css} from '@emotion/core';
import styled from '@emotion/styled';

import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import ProjectCard from './ProjectCard'


const ProjectsGallery = ({allProyectos}) => {

    const [projects,setProjects] = useState([]);
    const tittleRef = useRef(null);

    useEffect( () => {
        mixitup('.projects-cont')
        setProjects(allProyectos);
        if (typeof window !== `undefined`) {
            gsap.registerPlugin(ScrollTrigger)
        }
        
        gsap.from(tittleRef.current,{
            autoAlpha:0,
            y:20,
            ease:'power1.in',
            duration:1.2,
            scrollTrigger: {
                // id:'#tittleProjectsAnimId',
                trigger:tittleRef.current,
                start:'top center -=100',
            }
        });



    },)

    const TitleH1 = styled.h1`
        margin:0;

        @media only screen
        
        and (min-device-width: 200px)
        
        and (max-device-width: 767px)
        
        and (orientation: portrait) {
        
            font-size:1.5rem;
            margin-bottom:50px;
            
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 200px)
        
        and (max-device-width: 767px)
    
        
        and (orientation: landscape) {
            font-size:1.5rem;
            margin-bottom:50px;

        }

    `;

    const Gallery = styled.div`
        position:relative;
        top:0;
        left:0;
        right:0;
        bottom:0;
        margin:0 auto;
        width:100%;
        padding:0 3rem 3rem 3rem;
        display:inline-flex;
        justify-content:center;
        flex-wrap:wrap;
        transform-style: preserve-3d;
        perspective:1000px;
        perspective-origin:center;

        & > div {
            min-height: 80px;
        }
        @media only screen
        
        and (min-device-width: 150px)
        
        and (max-device-width: 359px)
        
        and (orientation: portrait) {
        
            padding:0rem !important;
            
        }

        @media only screen
        
        and (min-device-width: 360px)
        
        and (max-device-width: 767px)
        
        and (orientation: portrait) {
        
            padding:.5rem !important;
            
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 359px)
        
        and (max-device-width: 767px)
        
        and (orientation: landscape) {
        
            padding:.5rem !important;
        }

        @media only screen
        
        and (min-device-width: 768px)
        
        and (max-device-width: 1200px) {
        
            
            
        }

    `;
    const FilterButtons = styled.div`
        display:flex;
        width: 100%;
        padding:3rem 3rem 0 3rem;
        justify-content:space-around;
        

        button {
            padding:10px 20px;
            font-size:18px;
            font-weight:bold;
        }
        button.mixitup-control-active {
            background-color:#3FBF3F;
            color:white;
            border:2px black solid;
            border-radius:4px;
        }
        
        @media only screen
        
        and (min-device-width: 200px)
        
        and (max-device-width: 767px) {
            width:100%;
            height:auto;    
            overflow-y: hidden;
            
            justify-content:inherit;
            padding:3rem 0 0 0;

            button {
                font-size:15px;
            }
        }
    `;
    return ( 
        <section id="projects"  css={css`background:rgba(240,240,240,.9);text-align:center;`}>
            <TitleH1 id="tittleProjectsAnimId" className="CinzelTitle" ref={tittleRef}> PROJECTS </TitleH1>
            <FilterButtons>
                <button type="button" data-filter="all">All</button>
                <button type="button" data-toggle=".Payment">Payment</button>
                <button type="button" data-toggle=".PersonalProject">Personal Projects</button>
                <button type="button" data-toggle=".Development">On Development</button>
                <button type="button" data-toggle=".Done"> Done </button>
            </FilterButtons>
            <Gallery className="projects-cont">
                {projects.map((el) => (
                    
                    <ProjectCard 
                        key={el.id}
                        project={el}
                        
                    />
                ))}
            </Gallery>
        </section>
     );
}
 
export default ProjectsGallery;