import React, {useRef, useEffect} from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/core';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';


const MessageBanner = () => {

    const MainTextRef = useRef(null);

    useEffect( () => {

        let tl = gsap.timeline( {
            ease:'power1.out',
        });
        tl.from(MainTextRef.current,{autoAlpha:0,y:15,duration:1,delay:5.7})
        tl.to(MainTextRef.current, {
            y:0,
            autoAlpha:1,
            
        })
    }, [])

    const HeaderButton = styled.a`
        text-decoration:none;
        position:relative;
        z-index:200;
        border:2px solid #fff;
        padding:.5rem 2rem;
        align-self:center;
        background-color:transparent;
        color:white;
        transition:all 0.5s ease;
        &:hover {
            background-color:#3FBF3F;
            border:2px solid #3FBF3F;
        }
    `;
    const SectionHeader = styled.section`
        position:relative;
        display:flex;
        flex-wrap:wrap;
        justify-content:center;
        align-items:center;
        width:100%;
        height:100vh;


        h1 {
            color:white;
            text-shadow: 3px 2px 2px rgba(0, 0, 0, 0.79);
            font-size:calc(24px + 1vw);
        }
        span {
            color:#3FBF3F;
        }

        @media (min-device-width: 280px) and (max-device-width: 320px){
            width:100% !important;
            padding:0.8rem 0.8rem;
            h1 {
            font-size:1.3rem !important;
            width:100% !important;
            
            }

        }

        @media (min-device-width: 321px) and (max-device-width: 359px){
            width:100% !important;
            padding:0.8rem 0.8rem;
            h1 {
            /* font-size:1.3rem !important; */
            width:100% !important;
            
            }

        }

        @media only screen
        
        and (min-device-width: 360px)
        
        and (max-device-width: 667px)
        
        and (orientation: portrait) {
            padding:1rem 1rem;
            width:100% !important;
            h1 {
            /* font-size:1.5rem !important; */
            width:100% !important;
            }
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 360px)
        
        and (max-device-width: 667px)
        
        and (orientation: landscape) {
            width:100% !important;
            padding:1rem 1rem;
            h1 {
            /* font-size:1.5rem !important; */
            width:100% !important;
            }
            
        }
    `;
    return ( 

        <SectionHeader 
            
        >
            <div css={css`width:400px;`} ref={MainTextRef} id="MainAnimationId">
                <h1 className="CinzelTitle" css={css`display:block;text-align:center;`}>
                    Hi, I'm <span >Johan García</span> Frontend Developer
                </h1>
                <div css={css`display:flex;justify-content:center;`}>
                    <HeaderButton
                        download="johanG-CV.pdf"
                        href="/static/JohanCV2021.pdf"
                    > 
                        Download CV
                    </HeaderButton>
                </div>
            </div>
            
            
        </SectionHeader>
        
     );
}
 
export default MessageBanner;