import React, {useEffect, useRef} from 'react';
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import Lottie from 'react-lottie'
import animation from '../public/static/avatar.json';
import Skills from './Skills';
import Certificates from '../components/Certificates';

const About = ({allCerts}) => {

    const anim = useRef(null);
    const textAnim = useRef(null);
    const titleAnim = useRef(null)

    useEffect( () => {
        if (typeof window !== `undefined`) {
            gsap.registerPlugin(ScrollTrigger)
        }
        
        gsap.fromTo(titleAnim.current, {autoAlpha:0,y:20 }, {
            duration:.6,
            y:0,
            autoAlpha:1,
            ease:'power1.in',
            scrollTrigger: {
                id:'#tittleAnimId',
                trigger:titleAnim.current,
                start:'top center +=100',
            }
        })         
    }, [])

    const defaultOptions = {
        loop:true,
        autoplay:true,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        },
        animationData:animation,
        
    }
    const Container = styled.div`
        display: flex;
        flex-direction:column;

        @media 
        (min-width: 280px)
        
        and (max-width: 767px)
        
        and (orientation: portrait) {
        
            flex-wrap: wrap;
            margin-bottom:2rem;
        }
        
        /* Landscape */
        
        @media 
        (min-width: 280px)
        
        and (max-width: 767px)
        
        and (orientation: landscape) {
        
            flex-wrap: wrap;
            margin-bottom:2rem;
            
        }
    `;

    const AboutMe = styled.div`
        width:55%;
        height:auto;
        padding:0 10%;        
        font-size: 22px;
        align-self:flex-start;

        /*
        & > div > div img {
            width:100%;
            height:100%;
            border-radius:45%;
        } */
        
        #textAnimId {
            clip-path: polygon(20% 0%, 80% 0%, 100% 20%, 100% 80%, 93% 100%, 7% 100%, 0% 80%, 0% 20%);
        }

        @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 767px)
        
        and (orientation: portrait) {
        
            width:100% !important;
            padding:0 5% !important; 

            p {
                font-size:16px;
            }
            

            /* & > div > div {
            width:50%;
            height:50vh;
            
            }

            & > div > div img {
                width:100%;
                height:100%;
                border-radius:50%;
            } */
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 767px)
        
        and (orientation: landscape) {
        
            width:100% !important;
            padding:0 5% !important; 
            height:100vh;

            /* & > div > div {
                width:35%;
                height:60vh;
            
            }

            & > div > div img {
                width:100%;
                height:100%;
                border-radius:45%;
            } */
        }

        @media only screen
        
        and (min-device-width: 768px)
        
        and (max-device-width: 1200px) {
            /* & > div > div {
                width:50%;
                height:55vh;
            
            }

            & > div > div img {
                width:100%;
                height:100%;
                border-radius:45%;
            } */
            
            
        }
        
    `;

    

    return ( 
        <section id="about"  css={css`padding:6% 0 0 0;text-align:center;position:relative;`}>
            <h1 className="CinzelTitle" css={css`margin:0 0 80px 0;`} id="tittleAnimId" ref={titleAnim}>ABOUT</h1>
            <Container >
                <AboutMe>
                    <div ref={textAnim} id="textAnimId">
                            <Lottie id="firstLottie" height="300px" ref={anim} options={defaultOptions} />
                        
                        <p>
                            Frontend developer from Medellín ,Colombia.
                            Passionate about web technologies and constantly learn new stuff, if you have new challenges,
                            I am the right person.
                        </p>
                    </div>
                    
                </AboutMe>
                <Skills />
                <Certificates
                    allCerts={allCerts}
                />
            </Container>
            <div css={css`position:relative;bottom:0;z-index:-1;width:100%; `}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" css={css`position:absolute;bottom:0;left:0;right:0;`}>
                <path fill="#f1f1f1" fillOpacity="1" d="M0,64L80,101.3C160,139,320,213,480,234.7C640,256,800,224,960,186.7C1120,149,1280,107,1360,85.3L1440,64L1440,320L1360,320C1280,320,1120,320,960,320C800,320,640,320,480,320C320,320,160,320,80,320L0,320Z"></path>
            </svg>
            </div>
        </section>
     );
}
 
export default About;