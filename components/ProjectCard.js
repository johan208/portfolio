import React, {useRef} from 'react';
import {css} from '@emotion/core';
import styled from '@emotion/styled';
import { Image, renderMetaTags } from 'react-datocms'




const ProjectCard = ({project}) => {

    const cardRef = useRef(null);

    // const cardResponsive = css`
    //     media
    // `;

    const Button = styled.a`
        text-decoration:none;
        position:absolute;
        bottom:0;
        right:0;
        left:0;
        display:block;
        margin:0 auto;
        width:fit-content;
        border:2px solid #3FBF3F;
        padding:.5rem 1.5rem;
        background-color:transparent;
        color:black;
        transition:all .8s ease;
        z-index:100;
        &:hover {
            background-color:#3FBF3F;
            border:2px solid #3FBF3F;
            color:white;
            cursor:pointer;
        }
    `;
    const Card = styled.div`
        width:30%;
        position:relative;
        /* border-radius:15% 15% 0px 0px; */
        margin:2rem 1rem;
        z-index:100;
        box-shadow: 0px 20px 30px 0px rgba(0,0,0,0.4);
        -webkit-box-shadow: 0px 20px 30px 0px rgba(0,0,0,0.4);
        -moz-box-shadow: 0px 20px 30px 0px rgba(0,0,0,0.4);
        transition: .5s all cubic-bezier(0.445, 0.05, 0.55, 0.95);
        transform-style: preserve-3d;
        transform: rotatex(35deg) scale(1);
        
        @media only screen
        and (min-width: 200px)
        and (max-width: 767px)
        and (orientation: portrait) {
            width:100% !important;
            margin-bottom:2rem !important;
            border:none !important;
            transform: rotatex(0) scale(1);
            transform-style: flat;
        }
        /* Landscape */
        @media only screen
        
        and (min-width: 200px)
        and (max-width: 767px)
        and (orientation: landscape) {
            width:100% !important;
            margin-bottom:2rem !important;
            border:none !important;
            transform: rotatex(0) scale(1);
            transform-style: flat;
        }

        @media only screen
        and (min-width: 768px)
        and (max-width: 1200px) {
            width:45% !important;
            border:none !important;
        }

        &::after {
            content:'';
            width:85%;
            height:95%;
            z-index:-1;
            background: -webkit-linear-gradient(to right, #dce35b, #45b649); /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #dce35b, #45b649);
            position:absolute;
            left:0;
            bottom:0;
            filter:blur(15px);
        }
        
        /* flex-grow:1; */
        &:hover .back-side {
            opacity:1;
            background:rgba(240,240,240,.9);
            box-shadow: 0px 0px 30px 0px rgba(0,0,0,0);
            -webkit-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0);
            -moz-box-shadow: 0px 0px 30px 0px rgba(0,0,0,0);
        }
        &:hover .back-side .text,.backside-btn {
            opacity:1;
            transform:translateZ(100px);   
        }
        &:hover {
            transform: translateX(0) scale(1.1);

        }
        &:hover .back-side > .text{
            transform:translateY(30px) ;
        }

        &:hover .back-side > a{
            transform:translateY(-25px) ;
        }
        .back-side {
            position:absolute;
            top:0;
            left:0;
            width:100%;
            height:100%;
            opacity:0;
            text-align:center;
            transition: all ease .8s;
            transform-style: preserve-3d;
            z-index:100;
        }
        .card-img {
            width:100%;   
            height: 180px;
            &>div {
                width:100%;
                height:100%;
            }
            img {
                width:100%; 
                height:100%;
                object-fit:cover;
            }
        }
        .back-side .text {
            position:absolute;
            width:100%;
            transition: all ease 1s;
            transform:translateY(-10px) ;
        }
        .back-side .text {
             top:0;
             left:0;
        }
        .back-side div span{
             color:#3FBF3F;
        }
    `;

    return ( 
        <Card className={`mix ${project.category} ${project.state}`} ref={cardRef}>
            <div className=" back-side">
                <div className="text">
                    <div><b>{project.title}</b></div>
                    <span>{project.technologies}</span>
                </div>
                {
                    project.url == 'https://acurstin.com/apps/tecnoideas/transito/app/index.html'
                    ?
                    <Button 
                    className="backside-btn"
                    type="button"
                    href={project.url}
                    target="_blank"
                    onClick={() => alert('User: admin@gmail.com - pass: qwerty')}
                    >Ver Proyecto</Button>
                    :
                    <Button 
                    className="backside-btn"
                    type="button"
                    href={project.url}
                    target="_blank"
                    >Ver Proyecto</Button>
                }
                
                
            </div>
            
            <div className="card-img front-side">
                <Image data={project.img.responsiveImage} alt=""/>
                {/* <img src={project.img.url} /> */}
            </div>
            
            
            
        </Card>
     );
}
 
export default ProjectCard;