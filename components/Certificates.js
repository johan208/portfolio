import React,{useEffect, useRef} from 'react';
import {css,keyframes } from '@emotion/core';
import styled from '@emotion/styled';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { Image, renderMetaTags } from 'react-datocms'





const Certificates = ({allCerts}) => {

  const certContainer = useRef(null);
  const certTitle = useRef(null);

  useEffect( () => {
    if (typeof window !== `undefined`) {
        gsap.registerPlugin(ScrollTrigger)
    }

    let tl = gsap.timeline( {
      ease:'power1.in',
      scrollTrigger: {
        markers:false, 
          trigger:certContainer.current,
          start:'top +=500',
          end:'+=200',  
      },   
    });
    tl.fromTo(certTitle.current,{duration:.6,autoAlpha:0, y:20 },{autoAlpha:1,y:0 })
    .to(certContainer.current,{duration:40,rotationY:360,ease:'none',repeat:-1})
    


}, [certContainer.current])

  const CertGallery__Container = styled.section`

    position:relative;
    width: 100%;
    height:80%;
    display:flex;
    justify-content:center;
    align-items:center;
    perspective:950px;
    transform-style:preserve-3d;

      .certImg__container{
        position: relative;
        width:300px;
        height:200px;
        transform-style:preserve-3d;
        opacity:1;
      }
      .certImg__container > div{
        position: absolute !important;
        width:100%;
        height: 100%;
        top:0;
        left:0;
        transform-style:preserve-3d;
        transform-origin:center;
        transform:rotateY(calc(var(--i) * 72deg)) translateZ(280px);
        overflow:visible !important;
        -webkit-box-reflect:below 0px linear-gradient(transparent, transparent,#000d);
      }
      .certImg__container > div img{
        object-fit:cover;
      }

      @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 667px)
        
        and (orientation: portrait) {
          perspective:900px;
            
        }
        
  `;

  return ( 
    <>
    <section css={css`height:100vh;`}>
      {/* <h1 ref={certTitle} className="CinzelTitle" css={css`text-align:center;`}>CERTIFICATES</h1> */}
      <CertGallery__Container>

          <div className="certImg__container" ref={certContainer}>
            {allCerts.map((certificate,index) => (
              <Image  key={index} style={{ '--i': index +1}} data={certificate.certificateimg.responsiveImage} alt=""/>
            ))}
          </div>

      </CertGallery__Container>
    </section>
    </>
   );
}
 
export default Certificates;