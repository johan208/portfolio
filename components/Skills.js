import React, {useEffect, useRef} from 'react';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import {css} from '@emotion/core';
import styled from '@emotion/styled';


const Skills = () => {

    const skillref = useRef(null);

    const cssRef = useRef(null);
    const htmlRef = useRef(null);
    const jsRef = useRef(null);
    const reactRef = useRef(null);
    const vueRef = useRef(null);
    const nodeRef = useRef(null);
    const phpRef = useRef(null);
    const mongoRef = useRef(null);
    const mysqlRef = useRef(null);
    
    useEffect( () => {
        if (typeof window !== `undefined`) {
            gsap.registerPlugin(ScrollTrigger)
        }

        let tl = gsap.timeline( {
            ease:'power1.out',
            scrollTrigger: {
                // id:'#skillsId',
                trigger:skillref.current,
                start:'top +=500',
                end:'+=350',   
            },   
        });
        // tl.from(skillref.current,{duration:1,autoAlpha:0,x:+100,y:0})
        tl.to(skillref.current,{duration:1,autoAlpha:1,x:0,y: 0})
        tl.to(cssRef.current,{duration:0.3,width:'20%'})
        tl.to(htmlRef.current,{duration:0.3, width:'20%'},">")
        tl.to(jsRef.current,{duration:0.3, width:'20%'},">")
        tl.to(reactRef.current,{duration:0.3, width:'35%'},">")
        tl.to(vueRef.current,{duration:0.3, width:'35%'},">")
        tl.to(nodeRef.current,{duration:0.3, width:'45%'},">")
        // tl.to(phpRef.current,{duration:0.3, width:'100%'},">")
        tl.to(mongoRef.current,{duration:0.3, width:'55%'},">")
        tl.to(mysqlRef.current,{duration:0.3, width:'55%'},">")

    }, [skillref.current])

    const Skills = styled.div`
        width:50%;
        min-height:60vh;
        display:flex;
        justify-content:center;
        flex-wrap: wrap;
        align-self:flex-end;
        opacity:0;
        transform: translateX(100px);

        @media screen (min-width:668px) {
            transform: translateY(0);
        }

        @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 667px)
        
        and (orientation: portrait) {
            width:100%;
            margin-top:2rem;
            
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 667px)
        
        and (orientation: landscape) {
        
            flex-wrap:wrap !important;
            min-height:100vh;
        }

        &  div  {
            width:80%;   
            display:flex;
            
        }
        &  div span {
            height:25px;   
            background:#e4e4e4;
            display:flex;
            align-items:center;
            justify-content:flex-end;
            border-radius:0 30px 30px 0;
            padding-right:10px;
        }

        &  div > div {
            width:100%;
            height:25px;
            background:#3FBF3F;
            margin-bottom:30px;
            border-radius:30px 0 0 30px;
        }

        &  div > div > div {
            width:100px;
            height:25px;
            background:#0f1108;
            border-radius:30px 0 0 30px;
            color:white;
            display:flex;
            align-items:center;
            justify-content:center;
        }

    `;

    return ( 
        <Skills ref={skillref} id="skillsId">
            <div>
                <div >
                    <div>
                        <b className="CinzelTitle">Css</b>
                    </div>
                </div>
                <span css={css`width:350%;`} id="cssBarAnim" ref={cssRef}></span>
            </div>
            <div>
                <div>
                    <div>
                    <b className="CinzelTitle">Html</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={htmlRef}></span>
            </div>
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle"> Javascript</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={jsRef}></span>
            </div>
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle">React</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={reactRef}></span>
            </div>
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle">Vue</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={vueRef}></span>
            </div>
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle">NodeJs</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={nodeRef}></span>
            </div>
            {/* <div>
                <div>
                    <div>
                        <b>Php</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={phpRef}></span>
            </div> */}
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle">MongoDb</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={mongoRef}></span>
            </div>
            <div>
                <div>
                    <div>
                        <b className="CinzelTitle">MySql</b>
                    </div>
                </div>
                <span css={css`width:350%;`} ref={mysqlRef}></span>
            </div>
        </Skills>
     );
}
 
export default Skills;