import React from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/core';

import Background from './Background'
import MessageBanner from './MessageBanner'

const BackgroundWrapper = () => {

    return ( 
        <div id="home" css={css`position:relative;z-index:1;`}>

            <Background 
                
                // isanimated={isanimated}    
            />
            <MessageBanner 
            
            />

            {/* <div css={css`position:absolute;bottom:0;z-index:100;width:100%;`} >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320" css={css`position:absolute;bottom:0;left:0;right:0;`}>
                    <path fill="#ffffff" fill-opacity="1" d="M0,224L40,224C80,224,160,224,240,234.7C320,245,400,267,480,256C560,245,640,203,720,202.7C800,203,880,245,960,261.3C1040,277,1120,267,1200,266.7C1280,267,1360,277,1400,282.7L1440,288L1440,320L1400,320C1360,320,1280,320,1200,320C1120,320,1040,320,960,320C880,320,800,320,720,320C640,320,560,320,480,320C400,320,320,320,240,320C160,320,80,320,40,320L0,320Z"></path>
                </svg>
            </div> */}
            
        </div>
     );
}
 
export default BackgroundWrapper;