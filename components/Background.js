import React from 'react';
import styles from '../styles/background.module.css';
import {css,keyframes} from '@emotion/core';
import Particles from 'react-particles-js';

const Background = ({ isanimated }) => {
    
    const bg_img = css`
      background-image: url('/static/img/resized_background_escritorio.jpg');
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size:cover;
      background-position:center center;
      transition:clip-path 2.5s ease-in-out;
      overflow:hidden;
      position:absolute;
      top:0;
      left:0;
      right:0;
      bottom:0;
      width:100%;
      height:100vh;
      z-index:-1;

      @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 667px)
        
        and (orientation: portrait) {
          background-image: url('/static/img/background_mobil.jpg');
            
        }
        
        /* Landscape */
        
        @media only screen
        
        and (min-device-width: 280px)
        
        and (max-device-width: 667px)
        
        and (orientation: landscape) {
          background-image: url('/static/img/background_mobil.jpg');
        }

      &:before{
        content: "";
        width: 100%;
        height:100%;
        background: linear-gradient( to bottom, rgba(0, 0, 0, 0.50) 0%,rgba(0, 0, 0, 0.55) 0%, rgba(0, 0, 0, 0.6) 100% );
        position: absolute;
        z-index:2;
        
      }
    `
    return ( 
        <div>
            <Particles 
                // className={isanimated ? styles.animacionOn : styles.animacionOff}
                css={bg_img}
                params={
                    {
                        "particles": {
                          "number": {
                            "value": 10,
                            "density": {
                              "enable": true,
                              "value_area": 800
                            }
                          },
                          "color": {
                            "value": "#fd8b25"
                          },
                          "shape": {
                            "type": "circle",
                            "stroke": {
                              "width": 0,
                              "color": "#000000"
                            },
                            "polygon": {
                              "nb_sides": 6
                            },
                            "image": {
                              "src": "img/github.svg",
                              "width": 100,
                              "height": 100
                            }
                          },
                          "opacity": {
                            "value": 0.15,
                            "random": false,
                            "anim": {
                              "enable": false,
                              "speed": 1,
                              "opacity_min": 0.1,
                              "sync": false
                            }
                          },
                          "size": {
                            "value": 26.042650760819036,
                            "random": false,
                            "anim": {
                              "enable": true,
                              "speed": 6.744926547616142,
                              "size_min": 1.6241544246026904,
                              "sync": false
                            }
                          },
                          "line_linked": {
                            "enable": false,
                            "distance": 150,
                            "color": "#ffffff",
                            "opacity": 0.4,
                            "width": 1
                          },
                          "move": {
                            "enable": true,
                            "speed": 1,
                            "direction": "top",
                            "random": true,
                            "straight": false,
                            "out_mode": "out",
                            "bounce": false,
                            "attract": {
                              "enable": true,
                              "rotateX": 600,
                              "rotateY": 1200
                            }
                          }
                        },
                        "interactivity": {
                          "detect_on": "canvas",
                          "events": {
                            "onhover": {
                              "enable": true,
                              "mode": "repulse"
                            },
                            "onclick": {
                              "enable": true,
                              "mode": "bubble"
                            },
                            "resize": true
                          },
                          "modes": {
                            "grab": {
                              "distance": 400,
                              "line_linked": {
                                "opacity": 1
                              }
                            },
                            "bubble": {
                              "distance": 400,
                              "size": 40,
                              "duration": 2,
                              "opacity": 8,
                              "speed": 3
                            },
                            "repulse": {
                              "distance": 200,
                              "duration": 0.4
                            },
                            "push": {
                              "particles_nb": 4
                            },
                            "remove": {
                              "particles_nb": 2
                            }
                          }
                        },
                        "retina_detect": true
                      }
                }
            />
        </div>
     );
}
 
export default Background;