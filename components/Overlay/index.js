import React, {useEffect, useRef} from 'react';
import { gsap } from 'gsap';
import {OverlayCont, Loader, LoaderText} from './styled'

const Overlay = () => {
    const layerCont = useRef(null);
    const layer1 = useRef(null);
    const layer2 = useRef(null);
    const layer3 = useRef(null);
    const layer4 = useRef(null);
    const layer5 = useRef(null);
    const loader = useRef(null);
    const text = useRef(null);
    
    useEffect( () => {

        let tl = gsap.timeline( {
            ease:'power2.inOut',
        });
        tl.to(text.current,{duration:.1, autoAlpha:1})
        // .to(loader.current,{duration:.1, autoAlpha:1}) 
        // .to(loader.current,{duration:.6,rotate:'359deg',repeat: 5},"-=1")
        // .to(loader.current,{duration:.2, autoAlpha:0,delay:50})
        .to(text.current,{duration:.1, autoAlpha:0,delay: 5})
        .to(layer1.current,{duration:.7,width: '0px'},"<")
        .to(layer2.current,{duration:.7,width: '0px'}, "<")
        .to(layer3.current,{duration:.7, width: '0px'}, "<")
        .to(layer4.current,{duration:.7, width: '0px'}, "<")
        .to(layer5.current,{duration:.7, width: '0px'}, "<")
        .to(layerCont.current, {duration:0.1, y:'-200vh',delay:.8 })
        // .to(loader.current, {duration:0.1, y:'-200vh',autoAlpha:0 }, "<")
    }, [])

  return ( 
    <>
    <OverlayCont ref={layerCont}>
      <div ref={layer1}></div>
      <div ref={layer2}></div>
      <div ref={layer3}></div>
      <div ref={layer4}></div>
      <div ref={layer5}></div>
      <LoaderText className="CinzelTitle" ref={text}>
        <h2>Loading...</h2>
      </LoaderText>
    </OverlayCont>
    {/* <Loader ref={loader}>
    </Loader> */}

    </>
   );
}
 
export default Overlay;