import styled from '@emotion/styled';

export const OverlayCont = styled.div`
  width:100vw;
  height:100vh;
  position:fixed;
  top:0;
  left:0;
  z-index:111111;
  display:grid;
  grid-template-columns:1fr 1fr 1fr 1fr 1fr;
  grid-template-rows:1fr;
  background-color:transparent;
  /* perspective:1000px; 
  transform-style:preserve-3d; */

  & > div {
    background-color:white;
  }

`;
export const Loader = styled.div`
  opacity:0;
  position:fixed;
  top:50%;
  left:50%;
  transform:translate(-50%,-50%);
  width:80px;
  height:80px;
  border-radius:50%;
  border: 6px solid #3FBF3F;
  border-color: #3FBF3F transparent #3FBF3F transparent;
  background:transparent;
  z-index:111111;
  transform-style:preserve-3d;
  perspective:1000px;
`;
export const LoaderText = styled.div`
  position:absolute;
  top:50%;
  left:50%;
  transform:translate(-50%,-50%);
  font-weight:bold;
  color:black;
  z-index:111111111;
  -webkit-box-reflect: below -2px linear-gradient(transparent,#0002);

  &:after {
    content:'';
    position:absolute;
    width:2px;
    height:100%;
    right:0;
    top:0;
    background:black;
    animation: blinkCursor 0.4s steps(3) infinite;
  }
  @keyframes blinkCursor {
    0%,75% 
    {
      opacity:1;
    }
    76%,100% 
    {
      opacity:0;
    }
  }

  h2 {
    font-size:24px;
    letter-spacing:5px;
    padding:0;
    margin:0;
    animation:typing 4s steps(10) infinite;
    overflow:hidden;
    line-height:2.5rem;
  }

  @keyframes typing {
    0%,90%,100%
    {
      width:0;
    }
    30%,60%
    {
      width:169px;
    }
  }
`;
